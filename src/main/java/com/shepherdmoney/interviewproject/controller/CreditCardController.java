package com.shepherdmoney.interviewproject.controller;

import com.shepherdmoney.interviewproject.model.BalanceHistory;
import com.shepherdmoney.interviewproject.model.CreditCard;
import com.shepherdmoney.interviewproject.model.User;
import com.shepherdmoney.interviewproject.repository.CreditCardRepository;
import com.shepherdmoney.interviewproject.repository.UserRepository;
import com.shepherdmoney.interviewproject.vo.request.AddCreditCardToUserPayload;
import com.shepherdmoney.interviewproject.vo.request.UpdateBalancePayload;
import com.shepherdmoney.interviewproject.vo.response.CreditCardView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.temporal.ChronoUnit;
import java.util.*;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ResponseStatusException;



@RestController
public class CreditCardController {

    @Autowired
    UserRepository userRepository;
    // TODO: wire in CreditCard repository here (~1 line)
    @Autowired
    CreditCardRepository creditCardRepository;




    @PostMapping("/credit-card")
    public ResponseEntity<Integer> addCreditCardToUser(@RequestBody AddCreditCardToUserPayload payload) {
        // TODO: Create a credit card entity, and then associate that credit card with user with given userId
        //       Return 200 OK with the credit card id if the user exists and credit card is successfully associated with the user
        //       Return other appropriate response code for other exception cases
        //       Do not worry about validating the card number, assume card number could be any arbitrary format and length

        Optional<User> user = userRepository.findById(payload.getUserId());
        if(user.isEmpty()){
            return new ResponseEntity<>(payload.getUserId(), HttpStatus.NOT_FOUND);
        }
            CreditCard newCreditCard = new CreditCard();
            newCreditCard.setOwner(user.get());
            newCreditCard.setNumber(payload.getCardNumber());
            newCreditCard.setIssuanceBank(payload.getCardIssuanceBank());
            creditCardRepository.save(newCreditCard);
            return ResponseEntity.ok(newCreditCard.getId());
    }

    @GetMapping("/credit-card:all")
    public ResponseEntity<List<CreditCardView>> getAllCardOfUser(@RequestParam int userId) {
        // TODO: return a list of all credit card associated with the given userId, using CreditCardView class
        //       if the user has no credit card, return empty list, never return null
        Optional<User> user = userRepository.findById(userId);
        if(user.isEmpty()){
            return new ResponseEntity<>(new ArrayList<>(),HttpStatus.NOT_FOUND);
        }
        List<CreditCardView> creditCardViews = user.get().getCreditCards().stream().map(creditCard -> new CreditCardView(creditCard.getIssuanceBank(), creditCard.getNumber())).toList();

        return ResponseEntity.ok(creditCardViews);
    }

    @GetMapping("/credit-card:user-id")
    public ResponseEntity<Integer> getUserIdForCreditCard(@RequestParam String creditCardNumber) {
        // TODO: Given a credit card number, efficiently find whether there is a user associated with the credit card
        //       If so, return the user id in a 200 OK response. If no such user exists, return 400 Bad Request

        Optional<CreditCard> creditCard = creditCardRepository.findByNumber(creditCardNumber);
        return creditCard.map(card -> ResponseEntity.ok(card.getOwner().getId())).orElseGet(() -> ResponseEntity.badRequest().build());
    }

    @PostMapping("/credit-card:update-balance")
    public ResponseEntity postMethodName(@RequestBody UpdateBalancePayload[] payload) {
//        //TODO: Given a list of transactions, update credit cards' balance history.
//        //      1. For the balance history in the credit card
//        //      2. If there are gaps between two balance dates, fill the empty date with the balance of the previous date
//        //      3. Given the payload `payload`, calculate the balance different between the payload and the actual balance stored in the database
//        //      4. If the different is not 0, update all the following budget with the difference
//        //      For example: if today is 4/12, a credit card's balanceHistory is [{date: 4/12, balance: 110}, {date: 4/10, balance: 100}],
//        //      Given a balance amount of {date: 4/11, amount: 110}, the new balanceHistory is
//        //      [{date: 4/12, balance: 120}, {date: 4/11, balance: 110}, {date: 4/10, balance: 100}]
//        //      Return 200 OK if update is done and successful, 400 Bad Request if the given card number
//        //        is not associated with a card.
//
        // Sort transactions by date (ascending order)
        Arrays.sort(payload, Comparator.comparing(UpdateBalancePayload::getTransactionTime));

        // Process each transaction chronologically
        for (UpdateBalancePayload update : payload) {

            // Truncate transaction time to start of day
            update.setTransactionTime(update.getTransactionTime().truncatedTo(ChronoUnit.DAYS));

            // Find credit card by number
            CreditCard creditCard = creditCardRepository.findByNumber(update.getCreditCardNumber())
                    .orElseThrow(() -> {
                        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Credit card not found");
                    });


            // Get list of balance histories
            List<BalanceHistory> balanceHistories = creditCard.getBalanceHistories();

            // Find existing balance history for the same date
            BalanceHistory currBalanceHistory = balanceHistories.stream()
                    .filter(history -> history.getDate().equals(update.getTransactionTime()))
                    .findFirst()
                    .orElseGet(() -> new BalanceHistory(update.getTransactionTime(), 0, creditCard));

            // Update balance history
            currBalanceHistory.setBalance(currBalanceHistory.getBalance() + update.getTransactionAmount());

            // Save credit card with updated balance history
            creditCardRepository.save(creditCard);
        }

        return ResponseEntity.ok().build();
    }
    
}
